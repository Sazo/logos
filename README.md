# Repositorio de Logos Personales #

Estas obras pertenecen a Don Jorge Alejandro Sazo Pezoa y se encuentran licenciadas bajo la Licencia Creative Commons Atribución-SinDerivar 4.0 Internacional. Para ver una copia de esta licencia, visita http://creativecommons.org/licenses/by-nd/4.0/.

---------------------------------------------------------------------------------------------------------------------
# Repository for Personals Logos  #

These works belong to Don Jorge Alejandro Sazo Pezoa and are licensed under the Creative Commons Attribution 4.0 NoDerivatives International. To view a copy of this license, visit http://creativecommons.org/licenses/by-nd/4.0/.